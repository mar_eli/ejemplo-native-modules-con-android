import React, {useEffect} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

import {NativeModules, NativeEventEmitter} from 'react-native';
const {CalendarModule} = NativeModules;
// console.log(CalendarModule);
// CalendarModule.createCalendarEvent(res => console.log(res));

const eventEmitter = new NativeEventEmitter(CalendarModule);

const App = propos => {
  useEffect(() => {
    eventEmitter.addListener('EventCount', eventCount => {
      console.log(eventCount);
    });

    return () => {
      eventEmitter.removeAllListeners();
    };
  }, []);

  const createCalendarEventPromise = async () => {
    try {
      var result = await CalendarModule.createCalendarEventPromise();
      console.log(result);
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <View style={styles.container}>
      <Text>App</Text>
      <Button
        title="Calendar Event Promise"
        onPress={createCalendarEventPromise}
      />
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
